# Description:
# Generic classes for implementing a lidar
#
# Date:
# 02/02/2023
#
# Authors:
# Thibault Abry
# -------------------- #
# ---- HOW TO USE ---- #
#
# Inherit from Lidar and implement all of it's abstract methods.
# Your main program can now safely call the interface's methods to talk to your lidar.
# The Measurement class is used to represent 360° worth of data.
# -------------------- #

from abc import ABC, abstractmethod
import serial
from threading import Thread
import logging
from queue import Queue
from enum import Enum
import time

rpm = int
centidegree = int
mm = int
seconds = float
hz = int

class ThreadCommand(Enum):
    START = 0
    STOP = 1
    START_MEASURING = 2
    STOP_MEASURING = 3
    DISCONNECT = 4
    SET_SPEED = 5
    SET_ANGULAR_RESOLUTION = 6

class Measurement:
    
    def __init__(self, 
                 date_time: str, 
                 angular_resolution: centidegree, 
                 angles: list[centidegree], 
                 distances: list[mm]):
        self.date_time = date_time
        self.angular_resolution = angular_resolution
        self.angles = angles
        self.distances = distances
        
    

class Lidar(ABC, Thread):
    """Abstract class that represents a lidar.
    """
    
    def __init__(self, lidar_name: str):
        self.connected = False
        self.started = False
        self.measuring = False
        self.lidar_name = lidar_name
        self.command_queue = Queue() # Queue to send command to the thread
        Thread.__init__(self,name=f'{self.lidar_name} Thread',daemon=True)
    
    def run(self):
        self.__serialConnect()
        
    @abstractmethod
    def __serialConnect(self):
        """Opens the serial connection and calls the main loop.
        Must be overriden by the subclass.
        """
        pass
    
    def __addCommand(self, command: ThreadCommand, value: int):
        self.command_queue.put((command, value))
        
    def connect(self) -> bool:
        """Connects the serial port.
        
        Returns:
            bool: True if successful, False otherwise.
        """
        if self.connected:
            logging.error(f'{self.lidar_name} already connected on serial port.')
            return False
        self.start()
        return True

    def disconnect(self, timeout = 1) -> bool:
        """Disconnects the serial port.
        
        Args:
            timeout (float, optional): The method will block and wait for 
            the lidar to be ready for the specified time in seconds 
            before returning. Defaults to 1.
        Returns:
            bool: True if successful, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or self.measuring) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connect:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t disconnect.')
            return False
        
        if self.measuring:
            logging.error(f'{self.lidar_name} is measuring, can\'t disconnected.')
            return False
        
        self.__addCommand(ThreadCommand.DISCONNECT, None)
        return True
    
    # Begin() is used as a name instead of start() because start() is used by threading
    def begin(self, timeout = 1) -> bool:
        """Starts the lidar.

        Args:
            timeout (float, optional): The method will block and wait for 
            the lidar to be ready for the specified time in seconds before 
            returning. Defaults to 1.

        Returns:
            bool: True if successful, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t start.')
            return False
        if self.started:
            logging.warning(f'{self.lidar_name} is already started.')
            return False
            
        self.__addCommand(ThreadCommand.START, None)
        return True

    def stop(self,timeout = 1) -> bool:
        """Stops the Lidar and puts it to sleep mode.
             
        Returns:
            bool: True if successful, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started or self.measuring) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t stop.')
            return False
        if not self.started:
            logging.warning(f'{self.lidar_name} is not started, can\'t stop.')
        
        if self.measuring:
            logging.warning(f'{self.lidar_name} stopped while measuring.')
        
        self.__addCommand(ThreadCommand.STOP, None)
        return True
    
    def getInfo(self, timeout=1) -> str:
        """Gets information from the Lidar.

        Returns:
            str: The info sent by the Lidar, an empty string if a problem occured.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t get infos.')
            return ""
        if not self.started:
            logging.error(f'{self.lidar_name} is not started, can\'t get infos.')
            return ""       

        return self.info
    
    @abstractmethod
    def __checkAngularResolution(self, resolution:centidegree) -> bool:
        """Checks if the resolution given is within the accepted values.

        Args:
            resolution (centidegree): The resolution to check.

        Returns:
            bool: True if the resolution is accepted, false otherwise.
        """
        pass
    
    def setAngularResolution(self, resolution:centidegree, timeout=1) -> bool:
        """Sets the angular resolution of the Lidar in hundredth of degrees.

        Args:
            resolution (centidegree): The new desired angular resolution. Values depends on the Lidar: 
            
            | LS01B {25, 50, 100}
            | YLidarG4 {} (can't change)

        Returns:
            bool: True if speed has been correctly set, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t set angle resolution.')
            return False
        if not self.started:
            logging.error(f'{self.lidar_name} is not started, can\'t set angle resolution.')
            return False
        if not self.__checkAngularResolution(resolution):
            return False
        if self.measuring:
            logging.warning(f'Can\'t set {self.lidar_name}\'s angular resolution while measuring.'\
                           'Angular resolution hasn\'t changed.')
            return False
            
        self.__addCommand(ThreadCommand.SET_ANGULAR_RESOLUTION, resolution)
        return True
    
    @abstractmethod
    def __checkSpeed(self, speed: rpm) -> bool:
        """Checks if the speed given is within the accepted values.

        Args:
            speed (rpm): The speed to check.

        Returns:
            bool: True if the speed is accepted, false otherwise.
        """
        pass
    
    def setSpeed(self, speed: rpm, timeout=1) -> bool:
        """Sets the speed of the Lidar in rpm.

        Args:
            rpm (int): The new desired speed. Range depends on the Lidar: 
            
            | LS01B [180, 600]
            | YLidarG4 {318, 378, 438, 498, 558, 618, 678, 738}

        Returns:
            bool: True if speed has been correctly set, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t set speed.')
            return False
        if not self.started:
            logging.error(f'{self.lidar_name} is not started, can\'t set speed.')
            return False
        if not self.__checkSpeed(speed):
            return False
        if self.measuring:
            logging.error(f'Can\'t set {self.lidar_name}\'s speed while measuring.'\
                           'Speed hasn\'t changed.')
            return False
            
        self.__addCommand(ThreadCommand.SET_SPEED, speed)
        return True

    def startMeasuring(self, timeout = 1) -> bool:
        """Starts measuring data.

        Returns:
            bool: True if the Lidar started measuring, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t start measuring.')
            return False
        if not self.started:
            logging.error(f'{self.lidar_name} is not started, can\'t start measuring.')
            return False
        if self.measuring:
            logging.error(f'{self.lidar_name} is already measuring.')
            return False
                
        self.__addCommand(ThreadCommand.START_MEASURING, None)
        return True
    
    def stopMeasuring(self, timeout = 1) -> bool:
        """Stops measuring data.

        Returns:
            bool: True if the Lidar stoped measuring, False otherwise.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started or not self.measuring) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t stop measuring.')
            return False
        if not self.started:
            logging.warning(f'{self.lidar_name} is not started, can\'t stop measuring.')
        if not self.measuring:
            logging.warning(f'{self.lidar_name} is not measuring, can\'t stop measuring')
        
        self.__addCommand(ThreadCommand.STOP_MEASURING, None)
        return True
    
    def getMeasurement(self, timeout = 1) -> Measurement | None:
        """Gets a full 360° list of point.

        Returns:
            Measurement: 360° worth of data.
        """
        if timeout is not None:
            ms_counter = 0
            while (not self.connected or not self.started or not self.measuring) and ms_counter < timeout * 100:
                time.sleep(0.01)
                ms_counter += 1
        if not self.connected:
            logging.error(f'{self.lidar_name} is not connected to serial, can\'t get measurement.')
            return None
        if not self.started:
            logging.error(f'{self.lidar_name} is not started, can\'t get measurement.')
            return None
        if not self.measuring:
            logging.error(f'{self.lidar_name} is not measuring, can\'t get measurement.')
            return None
        
        return self.measuringThread.getMeasurement()
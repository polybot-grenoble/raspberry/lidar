# Date:
# 05/03/2023
#
# Authors:
# Thibault Abry
# -------------------- #
# ---- HOW TO USE ---- #
# -------------------- #

import serial
import time
from threading import Thread
import datetime
from queue import Queue
import math
import traceback

import logging
from .Lidar import Lidar, Measurement, ThreadCommand
from .Lidar import rpm, centidegree, mm, seconds, hz




# TODO: Handle unsollicited MeasuringThread terminations


# YLIDARG4 USES LITTLE ENDIAN AND LOW ORDER!!
# Doc:
# https://www.ydlidar.com/Public/upload/files/2022-06-21/YDLIDAR%20G4%20Development%20Manual%20V1.8(220411).pdf
# More commands:
# https://acroname.com/sites/default/files/assets/ydlidar_g4_development_manual_0.pdf
class YLidarG4(Lidar):
    
    NAME = "YLidarG4"
    # Speed of the serial communication (imposed by manufacturer)
    BAUD_RATE = 230400
    
    # Header of each sent command
    HEADER_SEND = 0xA5
    # Header of each received packet
    HEADER_RECEIVE = 0x5AA5 # In the doc, they write it using the little endian mode...
    
    HEALTH_CMD = bytearray([HEADER_SEND, 0x91]) # TODO: 92 or 91 ?
    GET_INFO_CMD = bytearray([HEADER_SEND, 0x90])
    
    LOW_POWER_CMD = bytearray([HEADER_SEND, 0x01])
    
    CONSTANT_FREQUENCY_CMD = bytearray([HEADER_SEND, 0x0E])
    
    SET_RANGING_FREQUENCY_CMD = bytearray([HEADER_SEND, 0xD0])
    GET_RANGING_FREQUENCY_CMD = bytearray([HEADER_SEND, 0xD1])
        
    INCREASE_SPEED_CMD = bytearray([HEADER_SEND, 0x0B])
    DECREASE_SPEED_CMD = bytearray([HEADER_SEND, 0x0C])
    GET_SPEED_CMD = bytearray([HEADER_SEND, 0x0D])
    
    START_CONTINUOUS_MEASURE_CMD = bytearray([HEADER_SEND, 0x60])
    STOP_CONTINUOUS_MEASURE_CMD = bytearray([HEADER_SEND, 0x65])
    
    class SerialLidar(object):
        def __init__(self, serial_port:int, baud_rate:int):
            self.serial_port = serial_port
            self.baud_rate = baud_rate
            
        def __enter__(self) -> serial.Serial:
            self.serial_device = serial.Serial(self.serial_port, self.baud_rate)
            return self.serial_device
        
        def __exit__(self, exc_type, exc_val, exc_tb):
            # Stop lidar
            self.serial_device.write(YLidarG4.STOP_CONTINUOUS_MEASURE_CMD)
            time.sleep(0.05)
            self.serial_device.close()
            
    class Header:
        """Class that represents a data packet header."""
            
        def __init__(self, header: bytes):
            """Creates a new header.

            Args:
                header (bytes): The first 7 bytes of the data packet.
            """
            self.start_sign = (header[1] << 8) + header[0]
            self.length = (header[5] << 24) + (header[4] << 16) + (header[3] << 8) + header[2]
            self.mode = self.length >> 30 # 2 bits for the mode
            self.length &= 0x2FFFFFFF # 30 bits for the length
            self.type_code = header[6]
            
        def isCorrect(self, length: int | None, mode: int, type_code: int) -> bool:
            """Checks if the 7 bytes of the header are correct.

            Args:
                length (int | None): The expected length. None if no length is expected.
                mode (int): The expected mode.
                type_code (int): The expected type code.

            Returns:
                bool: True if correct, False otherwise.
            """
            if self.start_sign != YLidarG4.HEADER_RECEIVE:
                logging.error(f'{YLidarG4.NAME} packet error: wrong header. '\
                          f'Expected {hex(YLidarG4.HEADER_RECEIVE)} but received {hex(self.start_sign)}')
                return False
            if length != None and self.length != length:
                logging.error(f'{YLidarG4.NAME} packet error: wrong length. '\
                          f'Expected {hex(length)} but received {hex(self.length)}')
                return False
            if self.mode != mode:
                logging.error(f'{YLidarG4.NAME} packet error: wrong mode. '\
                          f'Expected {hex(mode)} but received {hex(self.mode)}')
                return False
            if self.type_code != type_code:
                logging.error(f'{YLidarG4.NAME} packet error: wrong type_code. '\
                          f'Expected {hex(type_code)} but received {hex(self.type_code)}')
                return False
            
            return True

    def __init__(self, serial_port: str):
        self.measuringThread = None
        self.serial_port = serial_port
        Lidar.__init__(self, self.NAME)
        
    def _Lidar__serialConnect(self):
        # 1 second timeout means that if no data is received within 1 sec after calling 
        # serial.read() method, the call will timeout
        with self.SerialLidar(self.serial_port, self.BAUD_RATE) as serial_device:
            self.connected = True
            logging.info(f'{self.NAME} connected on serial port {self.serial_port}.')
            while self.__mainLoop(serial_device):
                pass
                
        logging.info(f'{self.NAME} disconnected from serial.')
        self.connected = False
        
    def __mainLoop(self, serial_device: serial.Serial) -> bool:
        """Main loop of the Lidar.

        Args:
            serial_device (serial.Serial): The Serial object used to communicate
            to the lidar.
        """
        threadCommand, value = self.command_queue.get(block=True, timeout=None)
        match threadCommand:
            case ThreadCommand.START:
                # Stop lidar
                #time.sleep(3)
                serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                time.sleep(0.05)
                serial_device.reset_input_buffer()
                serial_device.reset_output_buffer()
                time.sleep(0.1)
                # Checking health status
                try:
                    serial_device.write(self.HEALTH_CMD)
                    health_data = serial_device.read(10)
                    health_status, message = self.__decryptHealth(health_data)
                    if not health_status:
                        logging.critical(f'{self.NAME} bad health status. '\
                        f'Message: {message} '\
                        'Disconnecting serial.')
                        return False
                except Exception as e:
                    logging.critical(f'{self.NAME}\' serial connection to the lidar lost. '\
                                    'Couldn\'t retrieve health status. '\
                                    'Disconnecting serial.')
                    traceback.print_exc()
                    return False
                
                logging.info(f'{self.NAME}\' health status OK.')
                
                # Get information from the lidar
                serial_device.write(self.GET_INFO_CMD)
                info_data = serial_device.read(27)
                self.info = self.__decryptInfo(info_data)
                if self.info == "":
                    logging.critical(f'{self.NAME}\' serial connection to the lidar lost. '\
                                    'Couldn\'t retrieve information. '\
                                    'Disconnecting serial.')
                    return False

                # Enabling low power mode
                serial_device.write(self.LOW_POWER_CMD)
                response = serial_device.read(8)
                header = self.Header(response[0:7])
                if not header.isCorrect(0x00000001,0x0,0x04):
                    logging.error(f'{self.NAME} can\'t enter low power mode: wrong header. Disconnecting serial.')
                    return False
                if response[7] != 0x01:
                    logging.error(f'{self.NAME} can\'t enter low power mode: wrong response. Disconnecting serial.')
                    return False    
                
                logging.info(f'{self.NAME} low power mode enabled.')
                
                # Enabling constant frequency
                serial_device.write(self.CONSTANT_FREQUENCY_CMD)
                response = serial_device.read(8)
                header = self.Header(response[0:7])
                if not header.isCorrect(0x00000001,0x0,0x04):
                    logging.error(f'{self.NAME} can\'t set constant frequency: wrong header. Disconnecting serial.')
                    return False
                if response[7] != 0x01:
                    logging.error(f'{self.NAME} can\'t set constant frequency: wrong response. Disconnecting serial.')
                    return False
                
                logging.info(f'{self.NAME} constant frequency enabled.')

                # Setting 9000Hz ranging frequency (rate of measurements)
                if not self.__setRangingFrequency(serial_device,9000):
                    logging.critical(f'{self.NAME}\'s ranging frequency not correctly set. Disconnecting serial.')
                    return False
                
                logging.info(f'{self.NAME}\'s ranging frequency set to 9000Hz.')
                
                logging.info(f'{self.NAME} started.')
                self.started = True
            case ThreadCommand.STOP:
                serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                if self.measuring:
                    self.measuring = False
                    if self.measuringThread is not None:
                        self.measuringThread.kill()
                        logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                    else:
                        logging.info(f'{self.NAME} stopped measuring.')
                self.started = False
                logging.info(f'{self.NAME} stopped.')
            case ThreadCommand.START_MEASURING:
                serial_device.write(self.START_CONTINUOUS_MEASURE_CMD)
                response = serial_device.read(7)
                header = self.Header(response)
                if header.isCorrect(None, 0x1, 0x81):
                    self.measuringThread = MeasuringThread(serial_device)
                    self.measuringThread.start()
                    self.measuring = True
                    logging.info(f'{self.NAME} started measuring.')
                else:
                    logging.error(f'{self.NAME} can\'t start measuring: wrong header.')
            case ThreadCommand.STOP_MEASURING:
                serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                self.measuring = False
                if self.measuringThread is not None:
                    self.measuringThread.kill()
                    logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                else:
                    logging.info(f'{self.NAME} stopped measuring.')
            case ThreadCommand.DISCONNECT:
                return False
            case ThreadCommand.SET_SPEED:
                # Increase or decrease the speed one hz by one hz until it reaches
                # the desired speed.
                speed = value
                serial_device.write(self.GET_SPEED_CMD)
                response = serial_device.read(11)
                header = self.Header(response[0:7])
                if header.isCorrect(0x00000004,0x0,0x04):
                    current_speed = self.__convertSpeed(response[7:11])
                    logging.debug(f"First speed: {current_speed}, target: {speed}")
                    i = 0
                    problem = False
                    while i < 8 and current_speed != speed:
                        if current_speed < speed:
                            serial_device.write(self.INCREASE_SPEED_CMD)
                            logging.debug("Increasing speed")
                        else:
                            serial_device.write(self.DECREASE_SPEED_CMD)
                            logging.debug("Decreasing speed")

                        response = serial_device.read(11)
                        header = self.Header(response[0:7])
                        if not header.isCorrect(0x00000004,0x0,0x04):
                            logging.error(f'{self.NAME} can\'t set speed: wrong header.')
                            problem = True
                            break
                        current_speed = self.__convertSpeed(response[7:11])
                        logging.debug(f"New speed: {current_speed}")
                        i += 1
                    if i == 8:
                        logging.error(f'{self.NAME} problem while setting speed: full cycle. '\
                                      f'Last set speed: {current_speed}.')
                    elif problem:
                        logging.error(f"{self.NAME} problem while setting speed. "\
                                      f"Last set speed: {current_speed}")
                    else:
                        self.speed = speed
                        logging.info(f'{self.NAME}\'s speed set to {speed} rpm.')
                else:
                    logging.error(f'{self.NAME} can\'t get speed: wrong header.')
            case ThreadCommand.SET_ANGULAR_RESOLUTION:
                pass
        return True

    def __convertSpeed(self, speed: bytes) -> rpm:
        """Converts the 4 bytes speed into rpm.

        Args:
            speed (bytes): 4 bytes in little endian.

        Returns:
            rpm: The speed in rpm.
        """
        return ((speed[3] << 24) + (speed[2] << 16) + (speed[1] << 8) + speed[0]) / 100 * 60
    
    def __convertRangingFrequency(self, ranging: int) -> hz | None:
        """Converts the ranging frequency in hexadecimal to Hz.

        Args:
            ranging (int): 0x01, 0x02 or 0x03

        Returns:
            hz | None: 4000, 8000, 9000 respectively or None if ranging is not correct.
        """
        match ranging:
            case 0x00:
                return 4000
            case 0x01:
                return 8000
            case 0x02:
                return 9000
            case _:
                logging.error(f'{self.NAME} can\'t set ranging frequency: wrong response. '\
                                f'Expected 0x00 (4000Hz), 0x01 (8000Hz) or 0x02 (9000Hz) but received {ranging}.')
                return None
            
    def __setRangingFrequency(self, serial_device: serial.Serial, rangingFrequency: int) -> bool:
        """Cycles through the frequencies until the ranging frequency is correctly set"""
        serial_device.write(self.GET_RANGING_FREQUENCY_CMD)
        ranging_data = serial_device.read(8)
        header = self.Header(ranging_data[0:7]) # Data from first byte to the 7th
        if not header.isCorrect(0x00000001, 0x0, 0x04):
            logging.error(f'{self.NAME} can\'t get ranging frequency: wrong header')
            return False
        ranging = self.__convertRangingFrequency(ranging_data[7])
        
        i = 0
        while i < 3 and ranging != rangingFrequency:
            serial_device.write(self.SET_RANGING_FREQUENCY_CMD)
            response = serial_device.read(8)
            header = self.Header(response[0:7])
            if not header.isCorrect(0x00000001,0x0,0x04):
                logging.error(f'{self.NAME} can\'t set ranging frequency: wrong header')
                return False
            ranging = self.__convertRangingFrequency(response[7])
            i += 1
            
        if i == 3:
            logging.error(f'{self.NAME} can\'t set ranging frequency: full cycle. '\
                          f'Last set ranging frequency: {ranging}.')
            return False
        return True

    def __decryptHealth(self, health: bytes) -> tuple[bool, str]:
        """Decrypts the health data packet of the lidar.

        Args:
            health (bytes): The packet received.

        Returns:
            tuple[bool, str]: True if good health, false otherwise.
                              The message associated with the health status
        """
        header = self.Header(health[0:7])
        if not header.isCorrect(0x00000003,0x0, 0x06):
            logging.error(f'{self.NAME}\'s health status couldn\'t be retrieved: wrong header')
            return False, "Wrong header"
        
        status = health[7]
        good_health = status & 0x2F == 0
        sensor_status = status & 0x01 == 0
        encode_status = (status >> 1) & 0x01 == 0
        wireless_power_status = (status >> 2) & 0x01 == 0
        laser_feedback_voltage_status = (status >> 3) & 0x01 == 0
        laser_drive_current_status = (status >> 4) & 0x01 == 0
        data_status = (status >> 5) & 0x01 == 0
        
        message = f"{self.NAME}\'s health status: "
        if not sensor_status:
            message += "sensor status abnormal, "
        if not encode_status:
            message += "encode status abnormal, "
        if not wireless_power_status:
            message += "wireless power supply abnormal, "
        if not laser_feedback_voltage_status:
            message += "laser feedback voltage abnormal, "
        if not laser_drive_current_status:
            message += "laser drive curreny abnormal, "
        if not data_status:
            message += "data abnormal, "
        
        if good_health:
            message += "good health."
        else:
            message += "error code: "
            message += hex(status[8:10])
            message += "."
        
        return (good_health,message)  
        
    def __decryptInfo(self, info: bytes) -> str:
        header = self.Header(info[0:7])
        if not header.isCorrect(0x00000014,0x0, 0x04):
            logging.error(f'{self.NAME}\'s infos couldn\'t be retrieved: wrong header')
            return ""
        
        model = info[7]
        software_version = "V{0}.{1}".format(info[9], info[8])
        hardware_version = "V{0}".format(info[10])
        serial_number = "{0}".format(info[11:26])

        match model:
            case 0x05:
                model = "YLidarG4"
            case _:
                model = "Unknown"
        
        
        return f'{self.NAME}\'s infos: model-{model}; software-{software_version}; '\
               f'hardware-{hardware_version}; serial number-{serial_number}'

    def _Lidar__checkAngularResolution(self, resolution:centidegree) -> bool:
        logging.warning(f'{self.NAME} doesn\'t allow changing the angular resolution.')
        return False
        
    def _Lidar__checkSpeed(self, speed: rpm) -> bool:
        return speed == 318 or speed == 378 or speed == 438 or speed == 498 or speed == 558 or speed == 618 or speed == 678 or speed == 738


class MeasuringThread(Thread):
    """Threaded class that reads measurements from serial and makes it available.
    """
    
    def __init__(self, serial_device: serial.Serial):
        self.serial_device = serial_device
        self.angular_resolution = 1
        self.measurementsQueue = Queue(1)
        logging.debug(f'{YLidarG4.NAME}\'s measuring thread created')
        self.kill_flag = False # If set to true, the thread will exit
        Thread.__init__(self,name=f'{YLidarG4.NAME} Measuring Thread',daemon=True)
    
    def run(self):
        logging.debug(f'{YLidarG4.NAME}\'s measuring thread started')
        while self.__scan():
            continue
        logging.debug(f'{YLidarG4.NAME}\'s measuring thread terminated.')
        
    # Following code inspired by:
    # https://github.com/lakshmanmallidi/PyLidar3/blob/master/PyLidar3/__init__.py
    
    def __angleCorr(self,dist):
        if dist==0:
            return 0
        else:
            return (math.atan(21.8*((155.3-dist)/(155.3*dist)))*(180/math.pi))
        
    def __hexArrToDec(self,data):
        littleEndianVal = 0
        for i in range(0,len(data)):
            littleEndianVal = littleEndianVal+(data[i]*(256**i))
        return littleEndianVal
            
    def __calculate(self,d):
        ddict=[]
        LSN=d[1]
        Angle_fsa = ((self.__hexArrToDec((d[2],d[3]))>>1)/64.0)#+YdLidarX4._AngleCorr(YdLidarX4._HexArrToDec((d[8],d[9]))/4)
        Angle_lsa = ((self.__hexArrToDec((d[4],d[5]))>>1)/64.0)#+YdLidarX4._AngleCorr(YdLidarX4._HexArrToDec((d[LSN+6],d[LSN+7]))/4)
        if Angle_fsa<Angle_lsa:
            Angle_diff = Angle_lsa-Angle_fsa
        else:
            Angle_diff = 360+Angle_lsa-Angle_fsa
        for i in range(0,2*LSN,2):
            # Distance calculation
            dist_i = self.__hexArrToDec((d[8+i],d[8+i+1]))/4
            # Ignore zero values, they result in massive noise when
            # computing mean of distances for each angle.
            if dist_i == 0:
                continue
            # Intermediate angle solution
            Angle_i_tmp = ((Angle_diff/float(LSN-1))*(i/2))+Angle_fsa
            # Angle correction
            Angle_i_tmp += self.__angleCorr(dist_i)
            if Angle_i_tmp > 360:
                Angle_i = Angle_i_tmp-360
            elif Angle_i_tmp < 0:
                Angle_i = Angle_i_tmp+360
            else:
                Angle_i = Angle_i_tmp
            ddict.append((dist_i,Angle_i))
        return ddict
    
    def __checkSum(self,data):
        try:
            ocs = self.__hexArrToDec((data[6],data[7]))
            LSN = data[1]
            cs = 0x55AA^self.__hexArrToDec((data[0],data[1]))^self.__hexArrToDec((data[2],data[3]))^self.__hexArrToDec((data[4],data[5]))
            for i in range(0,2*LSN,2):
                cs = cs^self.__hexArrToDec((data[8+i],data[8+i+1])) 
            if(cs==ocs):
                return True
            else:
                return False
        except Exception as e:
            return False
        
    def __mean(self,data):
        if(len(data)>0):
            return int(sum(data)/len(data))
        return 0
    
    # TODO: Make a similar structure to the LS01B measuring thread
    def __scan(self):
        """Begin the lidar and returns a generator which returns a dictionary 
        consisting angle(degrees) and distance(meters).\n
        Return Format : 
        {angle(1):distance, angle(2):distance,....................,angle(360):distance}.
        """
        distdict = {}
        countdict = {}
        lastChunk = None
        while self.kill_flag is False:
            for i in range(0,360):
                distdict.update({i:[]})
            # Data is read 6000 bytes by 6000 bytes
            data = self.serial_device.read(6000).split(b"\xaa\x55")
            if lastChunk is not None:
                data[0] = lastChunk + data[0]
            lastChunk = data.pop()
            for e in data:
                try:
                    if(e[0]==0):
                        if(self.__checkSum(e)):
                            d = self.__calculate(e)
                            for ele in d:
                                angle = math.floor(ele[1])
                                if(angle>=0 and angle<360):
                                    distdict[angle].append(ele[0])
                except Exception as e:
                    pass
            for i in distdict.keys():
                if len(distdict[i]) > 0:
                    distdict[i]=self.__mean(distdict[i])
                else:
                    distdict[i]=0
                    
            angles = [None] * 360
            distances = [None] * 360
            
            for i,(angle, dist) in enumerate(distdict.items()):
                angles[i] = angle*100
                distances[i] = dist
                
                new_measurement = Measurement(datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"),
                                            self.angular_resolution,
                                            angles,
                                            distances)

            if self.measurementsQueue.full():
                self.measurementsQueue.get()
            self.measurementsQueue.put(new_measurement)

    def kill(self):
        """Sends the kill signal to terminate the thread.
        """
        self.kill_flag = True
            
    def getMeasurement(self) -> Measurement or None:
        """Gets the measurement and resets it to None.

        Returns:
            The current measurement, or None if it hasn't been created yet.
        """
        #measurement = self.measurement
        #self.measurement = None
        return self.measurementsQueue.get()
    
    
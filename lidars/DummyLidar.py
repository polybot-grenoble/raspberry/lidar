# Date:
# 29/04/2023
#
# Authors:
# Thibault Abry
# -------------------- #
# ---- HOW TO USE ---- #
# -------------------- #

import serial
import time
from threading import Thread
import datetime
from queue import Queue
import math
import traceback
from enum import Enum
import random


import logging
from .Lidar import Lidar, Measurement, ThreadCommand
from .Lidar import rpm, centidegree, mm, seconds, hz


class DummyMode(Enum):
    RANDOM = 1
    SINE = 2
    LINES = 3

class DummyLidar(Lidar):
    
    NAME = "DummyLidar"
    

    def __init__(self, dummy_mode: DummyMode, min_value:mm = 100, max_value:mm = 700):
        self.measuringThread = None
        self.dummy_mode = dummy_mode
        self.speed = 10 # 360° measures per seconds
        self.angular_resolution = 100 # centidegree
        self.min_value = min_value
        self.max_value = max_value
        Lidar.__init__(self, self.NAME)
        
    def _Lidar__serialConnect(self):
        logging.info(f'{self.NAME} connected.')
        while self.__mainLoop():
            pass
                
        logging.info(f'{self.NAME} disconnected from serial.')
        self.connected = False
        
    def __mainLoop(self) -> bool:
        """Main loop of the Lidar.
        """
        threadCommand, value = self.command_queue.get(block=True, timeout=None)
        match threadCommand:
            case ThreadCommand.START:
                self.info = "Dummy Lidar used for testing."
                logging.info(f'{self.NAME} started.')
                self.started = True
            case ThreadCommand.STOP:
                self.started = False
                logging.info(f'{self.NAME} stopped.')
            case ThreadCommand.START_MEASURING:
                self.measuringThread = MeasuringThread()
                self.measuringThread.start()
                self.measuring = True
                logging.info(f'{self.NAME} started measuring.')
            case ThreadCommand.STOP_MEASURING:
                self.measuring = False
                if self.measuringThread is not None:
                    self.measuringThread.kill()
                    logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                else:
                    logging.info(f'{self.NAME} stopped measuring.')
            case ThreadCommand.DISCONNECT:
                return False
            case ThreadCommand.SET_SPEED:
                self.speed = value
                logging.info(f'{self.NAME}\'s speed set to {self.speed} rpm.')
            case ThreadCommand.SET_ANGULAR_RESOLUTION:
                self.speed = value
                logging.info(f'{self.NAME}\'s speed set to {self.speed} rpm.')
                pass
        return True

    def _Lidar__checkAngularResolution(self, resolution:centidegree) -> bool:
        return True
        
    def _Lidar__checkSpeed(self, speed: rpm) -> bool:
        return True


class MeasuringThread(Thread):
    """Threaded class that reads measurements from serial and makes it available.
    """
    
    def __init__(self, angular_resolution: centidegree, speed: rpm, scan_mode: DummyMode, min_value:mm, max_value:mm):
        self.angular_resolution = angular_resolution
        self.speed = speed
        self.scan_mode = scan_mode
        self.angle_count = round(360 * (self.angular_resolution)/100)
        self.min_value = min_value
        self.max_value = max_value
        self.measurementsQueue = Queue(1)
        self.seed = random.randrange(0,1000)
        logging.debug(f'{DummyLidar.NAME}\'s measuring thread created')
        self.kill_flag = False # If set to true, the thread will exit
        Thread.__init__(self,name=f'{DummyLidar.NAME} Measuring Thread',daemon=True)
    
    def run(self):
        logging.debug(f'{DummyLidar.NAME}\'s measuring thread started')
        while self.__scan():
            continue
        logging.debug(f'{DummyLidar.NAME}\'s measuring thread terminated.')
        
    def __scan(self):

        angles = list(range(1,360,self.angular_resolution))
        distances = self.angle_count*[None]
        
        match(self.scan_mode):
            case DummyMode.RANDOM:
                for i, _ in enumerate(angles):
                    distances[i] = random.randrange(self.min_value, self.max_value)
                pass
            case DummyMode.SINE:
                for i, _ in enumerate(angles):
                    time = int(datetime.datetime.utcnow().timestamp())
                    distances[i] = self.min_value
                    distances[i] = math.sin(time)*(self.max_value-self.min_value)*0.5
                    distances[i] += 0.25*math.sin(time*2)*(self.max_value-self.min_value)
                pass
            case DummyMode.LINES:
                pass
                
        new_measurement = Measurement(datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"),
                                    self.angular_resolution,
                                    angles,
                                    distances)

        if self.measurementsQueue.full():
            self.measurementsQueue.get()
        self.measurementsQueue.put(new_measurement)

    def kill(self):
        """Sends the kill signal to terminate the thread.
        """
        self.kill_flag = True
            
    def getMeasurement(self) -> Measurement or None:
        """Gets the measurement and resets it to None.

        Returns:
            The current measurement, or None if it hasn't been created yet.
        """
        #measurement = self.measurement
        #self.measurement = None
        return self.measurementsQueue.get()
    
    
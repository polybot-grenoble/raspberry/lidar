# Date:
# 02/02/2023
#
# Authors:
# Leo Biales
# Julien Delphin
# Thibault Abry
# -------------------- #
# ---- HOW TO USE ---- #
#
# Instantiate a new LS01B with appropriate serial port (usually COMx) (see device manager)
# Call connect() method to connect to serial
# Call start() method to start the lidar
# From here you can safely call:
#  - getInfo() to get lidar's manufacturer info,
#  - setAngularResolution({25|50|100})
#  - setSpeed([180, 600])
#
# When ready, call startMeasuring() to start the measurements.
# WARNING: this will create a Thread to read on serial, thus disabling the getInfo() method.
# Calling getMeasure() will return an array representing 360° worth of data,
# and which size depends on the angular resolution (360 for 100, 720 for 50 and 1440 for 25).
# getMeasure() returns only the latest measure.

# Datasheet and other resources:
# https://www.mediafire.com/?rht3n4fzyaftx#x5adya824oi4y
# -------------------- #

import serial
import time
from threading import Thread
import datetime
from queue import Queue

import logging
from .Lidar import Lidar, Measurement, ThreadCommand
from .Lidar import rpm, centidegree, mm, seconds


    
# HOW THE CLASS IS BUILT:
# The LS01B inherits from Lidar, it implements its abstract methods.
# When connecting the lidar, a daemon thread is started to ensure that the
# serial connection is closed if any exception occurs.
#
# Each public method first checks if the call is possible, and then
# it adds a command to a queue which the thread reads from.
# 
# When begin() is called, another thread is started to get the measurements.

class LS01B(Lidar):
    
    NAME = "LS01B"
    # Speed of the serial communication (imposed by manufacturer)
    BAUD_RATE = 460800
    
    # Header of each command and response
    HEADER = 0xA5
    # Header of the first 30° of data received
    HEADER_START_PACKET = 0xA56A
    # Header of the next 30° of data received
    HEADER_NEXT_PACKET = 0xA55A
    
    START_CMD = bytearray([HEADER, 0x2C])
    STOP_CMD = bytearray([HEADER, 0x25])
    
    GET_INFO_CMD = bytearray([HEADER, 0x7F])
    
    SWITCH_DISTANCE_INTENSITY_CMD = bytearray([HEADER, 0x5C])
    
    SET_SPEED_CMD = bytearray([HEADER, 0x26])
    SET_ANGULAR_RESOLUTION_CMD = bytearray([HEADER, 0x30])
    
    START_CONTINUOUS_MEASURE_CMD = bytearray([HEADER, 0x20])
    STOP_CONTINUOUS_MEASURE_CMD = bytearray([HEADER, 0x21])
    
    class SerialLidar(object):
        def __init__(self, serial_port:int, baud_rate:int):
            self.serial_port = serial_port
            self.baud_rate = baud_rate
            self.restart = False
            
        def __enter__(self) -> serial.Serial:
            self.serial_device = serial.Serial(self.serial_port, self.baud_rate, timeout=1)
            return self.serial_device
        
        def __exit__(self, exc_type, exc_val, exc_tb):
            # Stop lidar
            self.serial_device.write(LS01B.STOP_CONTINUOUS_MEASURE_CMD)
            time.sleep(0.05)
            self.serial_device.write(LS01B.STOP_CMD)
            time.sleep(0.05)
            self.serial_device.close()

    def __init__(self, serial_port: str):
        self.measuringThread = None
        self.restart = False
        self.serial_port = serial_port
        Lidar.__init__(self, self.NAME)
        
    def _Lidar__serialConnect(self):
        # 1 second timeout means that if no data is received within 1 sec after calling 
        # serial.read() method, the call will timeout
        with self.SerialLidar(self.serial_port, self.BAUD_RATE) as serial_device:
            self.connected = True
            logging.info(f'{self.NAME} connected on serial port {self.serial_port}.')
            while self.__mainLoop(serial_device):                    
                pass
                
        logging.info(f'{self.NAME} disconnected from serial.')
        self.connected = False
        if self.restart is True:
            self.restart = False
            self._Lidar__serialConnect()
        
    def __flushBuffers(self, serial_device: serial.Serial) -> bool:
        serial_device.flushInput()
        serial_device.flushOutput()
        while(serial_device.in_waiting != 0):
            logging.debug("Waiting for buffer to flush")
            time.sleep(0.01)
            serial_device.flushInput()
            serial_device.flushOutput()
        
    def __mainLoop(self, serial_device: serial.Serial) -> bool:
        """Main loop of the Lidar.

        Args:
            serial_device (serial.Serial): The Serial object used to communicate
            to the lidar.
        """
        threadCommand, value = (None, None)
        while True:
                        
            try:
                threadCommand, value = self.command_queue.get(block=False)
                logging.debug(f"Treating command {threadCommand}")
                break
            except:
                if self.measuringThread is not None:
                    #logging.debug(f"measuring thread: {self.measuringThread is not None}, alive: {self.measuringThread.is_alive()}, measuring: {self.measuring}")
                    time.sleep(0.1)
                # Last check might be unnecessary
                if self.measuringThread is not None and not self.measuringThread.is_alive() and self.measuring and self.measuringThread.kill_flag is False:
                    logging.info(f"{LS01B.NAME} is restarting.")
                    self.restart = True
                    self._Lidar__addCommand(ThreadCommand.START, None)
                    self._Lidar__addCommand(ThreadCommand.START_MEASURING, None)
                    self.measuring = False
                    self.started = False
                    return False
                
                
        if threadCommand is None:
            return True
        
        match threadCommand:
            case ThreadCommand.START:
                self.__flushBuffers(serial_device)
                    
                # Stop lidar
                serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                time.sleep(0.05)
                serial_device.write(self.STOP_CMD)
                time.sleep(0.05)

                serial_device.write(self.START_CMD)
                logging.info(f'{self.NAME} started.')
                time.sleep(0.05) # Recommended by the manufacturer
                self.__flushBuffers(serial_device)
                
                # Get information from the lidar:
                try:
                    serial_device.write(self.GET_INFO_CMD)
                    info = serial_device.read(9)
                    self.info = self.__decryptInfo(info)
                    if self.info == "":
                        return False
                except Exception as e:
                    logging.critical(f'{self.NAME}\' serial connection to the lidar lost. '\
                                    'Couldn\'t retrieve information. '\
                                    'Disconnecting serial.')
                    return False
                self.started = True
            case ThreadCommand.STOP:
                if self.measuring:
                    serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                    self.measuring = False
                    if self.measuringThread is not None:
                        self.measuringThread.kill()
                        logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                    else:
                        logging.info(f'{self.NAME} stopped measuring.')
                    time.sleep(0.05)
                serial_device.write(self.STOP_CMD)
                self.started = False
                logging.info(f'{self.NAME} stopped.')
            case ThreadCommand.START_MEASURING:
                serial_device.write(self.START_CONTINUOUS_MEASURE_CMD)
                self.measuringThread = MeasuringThread(serial_device,self.angular_resolution)
                self.measuringThread.start()
                self.measuring = True
                logging.info(f'{self.NAME} started measuring.')
            case ThreadCommand.STOP_MEASURING:
                serial_device.write(self.STOP_CONTINUOUS_MEASURE_CMD)
                self.measuring = False
                if self.measuringThread is not None:
                    self.measuringThread.kill()
                    logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                else:
                    logging.info(f'{self.NAME} stopped measuring.')

            case ThreadCommand.DISCONNECT:
                return False
            case ThreadCommand.SET_SPEED:
                # Write to serial the set_speed command + 2 bytes 
                # for the new speed in rpm
                speed = value
                command = bytearray(self.SET_SPEED_CMD)
                command.extend(speed.to_bytes(2, 'big'))
                serial_device.write(command)
                self.speed = speed
                logging.info(f'{self.NAME}\'s speed set to {speed} rpm.')
            case ThreadCommand.SET_ANGULAR_RESOLUTION:
                # Write to serial the set_angular_resolution command + 2 bytes 
                # for the new resolution in centidegrees
                resolution = value
                command = bytearray(self.SET_ANGULAR_RESOLUTION_CMD)
                command.extend(resolution.to_bytes(2,'big'))
                serial_device.write(command)
                self.angular_resolution = resolution
                logging.info(f'{self.NAME}\'s angular resolution set to {resolution} centidegrees.')
        return True


    def __decryptInfo(self, info: bytes) -> str:
        header = info[0]
        if header != self.HEADER:
            logging.error(f'{self.NAME}\'s infos couldn\'t be retrieved: wrong header. '\
                          f'Expected 0xa5 but received {hex(header)}')
            return ""
        
        model = (info[1] << 8) + info[2]
        software_version = "{0:#04X}".format((info[3] << 8) + info[4])
        hardware_version = "{0:#04X}".format((info[5] << 8) + info[6])

        match model:
            case 0x013B:
                model = "LS01B3"
            case 0x012B:
                model = "LS01B2"
            case 0x011B:
                model = "LS01B1"
            case _:
                model = "Unknown"
        
        software_version = "V{0}.{1}.{2}".format(
                                          software_version[2],
                                          software_version[3],
                                          software_version[4:5])
        
        hardware_version = "V{0}.{1}.{2}".format(
                                          hardware_version[2],
                                          hardware_version[3],
                                          hardware_version[4:5])
        
        return f'{self.NAME}\'s infos: model-{model}; software-{software_version}; hardware-{hardware_version}'

    def _Lidar__checkAngularResolution(self, resolution:centidegree) -> bool:
        correct = resolution == 25 or resolution == 50 or resolution == 100
        if not correct:
            logging.error(f'Can\'t set {self.lidar_name}\'s angular resolution to {resolution} centidegrees. '\
                    'Angular resolution must be 25, 50 or 100 centidegree. '\
                    'Angular resolution hasn\'t changed.')
        return correct
        
    def _Lidar__checkSpeed(self, speed: rpm) -> bool:
        correct = speed >= 180 and speed <= 600
        if not correct:
            logging.error(f'Can\'t set {self.lidar_name}\'s speed to {speed} rpm. '\
                            'Speed must be in the range [180, 600] rpm. '\
                            'Speed hasn\'t changed.')
        return correct


class MeasuringThread(Thread):
    """Threaded class that reads measurements from serial and makes it available.
    """
    
    def __init__(self, serial_device: serial.Serial, angular_resolution: centidegree):
        self.serial_device = serial_device
        self.angular_resolution = angular_resolution
        self.angles_count = int(15 / (float(angular_resolution) / 100))
        self.packet_length = 6 + int(3 * self.angles_count)
        self.measurementsQueue = Queue(1)
        
        self.kill_flag = False # If set to true, the thread will exit
        self.wait_next = False
        Thread.__init__(self,name=f'{LS01B.NAME} Measuring Thread',daemon=True)
    
    def run(self):
        logging.debug(f'{LS01B.NAME}\'s measuring thread started')
        while self.readData():
            continue
        logging.debug(f"{LS01B.NAME}\'s measuring thread stopped")
            
    def readData(self) -> bool:
        new_measurement = None
        for packet_count in range(24):
            raw_data = []
            current_packet_length = self.packet_length
            
            while len(raw_data) == 0:
                raw_data = None
                try:
                    raw_data = self.serial_device.read(current_packet_length)
                except Exception as e:
                    # If read raises an exception, it can mean 2 things:
                    #  - We sent a kill signal
                    #  - The connection was lost or another error occured
                    # In the latter we raise a critical error because the lidar is malfunctionning
                    # In both cases we exit the thread
                    if self.kill_flag: # Thread received a signal to terminate
                        logging.info(f'{LS01B.NAME}\'s measuring thread terminated.')
                    else:
                        logging.critical(f'{LS01B.NAME}\'s serial connection to the lidar lost. '\
                                        'Measuring thread terminated.')
                        logging.critical(e, exc_info=True)
                    return False
            
            if len(raw_data) != current_packet_length:
                error_format = "Received {>4d} but expected {>4d}. "\
                                .format(len(raw_data),current_packet_length)
                logging.critical(f'{LS01B.NAME}\'s packet length not correct. '\
                                f'{error_format}'\
                                'Measuring thread terminated.')
                return False
            
            decrypted_data = self.__decryptData(raw_data)
            #logging.debug(f'Decrypted angles starting at {decrypted_data.angles[0]}')
            # FIXME: The lidar should be able to reset itself so it can measure again
            if decrypted_data == None:
                logging.info(f"{LS01B.NAME}\'s measuring thread is exiting.")
                return False
            if new_measurement == None:
                new_measurement = decrypted_data
            else:
                new_measurement.angles.extend(decrypted_data.angles)
                new_measurement.distances.extend(decrypted_data.distances)
                
        #logging.debug(f'Got {len(new_measurement.angles)} angles')    
        if self.measurementsQueue.full():
            self.measurementsQueue.get()
        self.measurementsQueue.put(new_measurement)
        return True

    def __decryptData(self, raw_data: bytes) -> Measurement or None:
        """Decrypts the raw data passed in argument to a comprehensive measurement.

        Args:
            raw_data (bytes): Raw data bytes.

        Returns:
            Measurement or None: 15° worth of data.
        """
        # Data format:
        # 0xA5
        # 0x6A -> start of the 360° measures | 0x5A -> next measures
        # strength_flag, speed[14:8]
        # speed[7:0]
        # angular_resolution[6:0]
        
        header = (raw_data[0] << 8) + raw_data[1]
        #logging.debug("Header: "+ hex(header))
        if header != LS01B.HEADER_START_PACKET and header != LS01B.HEADER_NEXT_PACKET:
            logging.error(f'{LS01B.NAME} Wrong header format. '\
                          f'Expected 0xa65a or 0xa55a but received {hex(header)}. '\
                          f'Raw first bytes: {raw_data[0]} {raw_data[1]} {raw_data[2]}.')
            return None
        
        speed = (raw_data[2] << 8) + raw_data[3]
        # True if the lidar is in strength mode instead of normal
        isStrength = speed >> 15 == 1
        speed &= ~(0x01 << 15) # Setting the last bit to 0
        
        start_angle = (raw_data[4] << 8) + raw_data[5]
        angular_resolution = start_angle >> 9
        start_angle &= ~(0x7F << 9) # Setting the last 7 bits to 0
        
        if angular_resolution != self.angular_resolution:
            logging.warning(f'{LS01B.NAME}\'s angular resolution changed during measurement. '\
                            f'Expected {self.angular_resolution} but received {angular_resolution}. '\
                             'Data may be incorrect.')


        angles = [0] * self.angles_count
        distances = [0] * self.angles_count
        
        #logging.debug(f'Decrypting angles starting at {start_angle * 100}')
        for i in range(self.angles_count):
            # The angle value received is the offset from the start_angle in terms of angular_resolution
            # e.g. if angular_resolution = 50, angle = start_angle + value_received * 0.5
            # Note: we must convert start_angle in centidegree to stay consistent
            angles[i] = raw_data[i * 3 + 6] * self.angular_resolution + start_angle * 100
            # The distance is directly encoded in mm
            distances[i] = (raw_data[i * 3 + 7] << 8) + raw_data[i * 3 + 8]
            
        return Measurement(datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"),
                           self.angular_resolution,
                           angles,
                           distances)

    def kill(self):
        """Sends the kill signal to terminate the thread.
        """
        self.kill_flag = True
            
    def getMeasurement(self) -> Measurement or None:
        """Gets the measurement and resets it to None.

        Returns:
            The current measurement, or None if it hasn't been created yet.
        """
        #measurement = self.measurement
        #self.measurement = None
        meas = None
        try:
            meas = self.measurementsQueue.get(timeout=0.1)
        except:
            logging.info(f'{LS01B.NAME}\'s has no measurement available.')
            return None
        return meas
    
    

import serial
import time
from threading import Thread
import datetime
from queue import Queue

import logging
from Lidar import Lidar, Measurement, ThreadCommand
from Lidar import rpm, centidegree, mm, seconds
from PyLidar3 import YdLidarG4

# YLIDARG4 that uses the Pylidar3 library

class YLidarG4(Lidar):
    
    NAME = "YLidarG4"
    # Speed of the serial communication (imposed by manufacturer)
    BAUD_RATE = 128000
    

    def __init__(self, serial_port: str):
        self.measuringThread = None
        self.serial_port = serial_port
        Lidar.__init__(self, self.NAME)
        
    def _Lidar__serialConnect(self):
        lidar = YdLidarG4(self.serial_port)
        logging.debug("Serial connect called")
        if(lidar.Connect()):
            logging.debug("Serial connect called")

            self.connected = True
            logging.info("Lidar Connected")
            while self.__mainLoop(lidar):
                pass                    
        else:
            logging.critical("Error connecting lidar")
        lidar.Disconnect()

        
    def __flushBuffers(self, serial_device: serial.Serial) -> bool:
        pass
        
    def __mainLoop(self, lidar) -> bool:
        """Main loop of the Lidar.

        Args:
            serial_device (serial.Serial): The Serial object used to communicate
            to the lidar.
        """
        threadCommand, value = (None, None)
        while True:
                        
            try:
                threadCommand, value = self.command_queue.get(block=False)
                logging.debug(f"Treating command {threadCommand}")
                break
            except:
                if self.measuringThread is not None:
                    #logging.debug(f"measuring thread: {self.measuringThread is not None}, alive: {self.measuringThread.is_alive()}, measuring: {self.measuring}")
                    time.sleep(0.1)
                # Last check might be unnecessary
                if self.measuringThread is not None and not self.measuringThread.is_alive() and self.measuring and self.measuringThread.kill_flag is False:
                    logging.info(f"{YLidarG4.NAME} is restarting.")
                    self.restart = True
                    self._Lidar__addCommand(ThreadCommand.START, None)
                    self._Lidar__addCommand(ThreadCommand.START_MEASURING, None)
                    self.measuring = False
                    self.started = False
                    return False
                
                
        if threadCommand is None:
            return True
        
        match threadCommand:
            case ThreadCommand.START:
                    
                # Stop lidar

                logging.info(f'{self.NAME} started.')
                time.sleep(0.05) # Recommended by the manufacturer
                
                # Get information from the lidar:
                try:
                    self.info = lidar.GetDeviceInfo()
                except Exception as e:
                    logging.critical(f'{self.NAME}\' serial connection to the lidar lost. '\
                                    'Couldn\'t retrieve information. '\
                                    'Disconnecting serial.')
                    return False
                self.started = True
            case ThreadCommand.STOP:
                if self.measuring:
                    lidar.StopScanning()
                    if self.measuringThread is not None:
                        self.measuringThread.kill()
                        logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                    else:
                        logging.info(f'{self.NAME} stopped measuring.')
                    time.sleep(0.05)
                self.started = False
                logging.info(f'{self.NAME} stopped.')
            case ThreadCommand.START_MEASURING:
                lidar.StartScanning()
                self.measuringThread = MeasuringThread(lidar,self.angular_resolution)
                self.measuringThread.start()
                self.measuring = True
                logging.info(f'{self.NAME} started measuring.')
            case ThreadCommand.STOP_MEASURING:
                lidar.StopScanning()
                self.measuring = False
                if self.measuringThread is not None:
                    self.measuringThread.kill()
                    logging.info(f'{self.NAME} sent kill signal to measuring thread.')
                else:
                    logging.info(f'{self.NAME} stopped measuring.')

            case ThreadCommand.DISCONNECT:
                return False
            case ThreadCommand.SET_SPEED:
                # Write to serial the set_speed command + 2 bytes 
                # for the new speed in rpm
                speed = value
                
                self.speed = speed
                logging.info(f'{self.NAME}\'s speed set to {speed} rpm.')
            case ThreadCommand.SET_ANGULAR_RESOLUTION:
                # Write to serial the set_angular_resolution command + 2 bytes 
                # for the new resolution in centidegrees
                resolution = value
                
                self.angular_resolution = resolution
                logging.info(f'{self.NAME}\'s angular resolution set to {resolution} centidegrees.')
        return True


    def __decryptInfo(self, info: bytes) -> str:
        pass

    def _Lidar__checkAngularResolution(self, resolution:centidegree) -> bool:
        correct = resolution == 100
        if not correct:
            logging.error(f'Can\'t set {self.lidar_name}\'s angular resolution to {resolution} centidegrees. '\
                    'Angular resolution must be 100 centidegree. '\
                    'Angular resolution hasn\'t changed.')
        return correct
        
    def _Lidar__checkSpeed(self, speed: rpm) -> bool:
        correct = False
        if not correct:
            logging.error(f'Can\'t set {self.lidar_name}\'s speed to {speed} rpm. '\
                            'Can\'t change speed. '\
                            'Speed hasn\'t changed.')
        return correct


class MeasuringThread(Thread):
    """Threaded class that reads measurements from serial and makes it available.
    """
    
    def __init__(self, lidar: serial.Serial, angular_resolution: centidegree):
        self.lidar = lidar
        self.angular_resolution = angular_resolution
        self.angles_count = int(15 / (float(angular_resolution) / 100))
        self.packet_length = 6 + int(3 * self.angles_count)
        self.measurementsQueue = Queue(1)
        
        self.kill_flag = False # If set to true, the thread will exit
        self.wait_next = False
        Thread.__init__(self,name=f'{YLidarG4.NAME} Measuring Thread',daemon=True)
    
    def run(self):
        logging.debug(f'{YLidarG4.NAME}\'s measuring thread started')
        gen = self.lidar.StartScanning()

        while self.readData(gen):
            if self.kill_flag: # Thread received a signal to terminate
                logging.info(f'{YLidarG4.NAME}\'s measuring thread terminated.')
                return
                        
            continue
        logging.debug(f"{YLidarG4.NAME}\'s measuring thread stopped")
            
    def readData(self, gen) -> bool:
        new_measurement = None
        
        
        data = next(gen)
        
        angles = [None] * 360
        distances = [None] * 360
        
        for i,(angle, dist) in enumerate(data.items()):
            angles[i] = angle*100
            distances[i] = dist
            
            new_measurement = Measurement(datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"),
                                          self.angular_resolution,
                                          angles,
                                          distances)
        #logging.debug(f'Got {len(new_measurement.angles)} angles')    
        if self.measurementsQueue.full():
            self.measurementsQueue.get()
        self.measurementsQueue.put(new_measurement)
        return True

    def kill(self):
        """Sends the kill signal to terminate the thread.
        """
        self.kill_flag = True
            
    def getMeasurement(self) -> Measurement or None:
        """Gets the measurement and resets it to None.

        Returns:
            The current measurement, or None if it hasn't been created yet.
        """
        #measurement = self.measurement
        #self.measurement = None
        meas = None
        try:
            meas = self.measurementsQueue.get(timeout=0.1)
        except:
            logging.info(f'{YLidarG4.NAME}\'s has no measurement available.')
            return None
        return meas
    
    
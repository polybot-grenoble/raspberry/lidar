import numpy as np
import cv2
from lidars.Lidar import Measurement
import math
import logging


class Display:

    TARGET_COLOR = (255, 255, 255)
    DEFAULT_MM_SIZE = 100

    FONT = cv2.FONT_HERSHEY_SIMPLEX
    FONT_SIZE = 12
    FONT_SCALE = 1/25*FONT_SIZE

    def __init__(self, lidar_name: str, pixel_size: tuple[int, int], mm_size: tuple[int | None, int | None]):
        """Creates a new Display with given size in pixel and millimeters.

        Args:
            pixel_size (tuple[int, int]): The width and height of the image in pixels.
            mm_size (tuple[int  |  None, int  |  None]): The width or length of the image in millimeters.
                If the width is given and the height is None, the scale will be calculated from the width.

                If the height is given and the width is None, the scale will be calculated from the height.

                If both are given, or both are None, the scale will be calculated from the height using the
                default value.
        """

        self.pixel_size = pixel_size

        self.xmax, self.ymax = pixel_size

        mm_xmax, mm_ymax = mm_size

        if mm_xmax is not None and mm_ymax is None:
            self.mm_size = mm_xmax
            self.mm_to_pixels = self.xmax/self.mm_size
        elif mm_ymax is not None and mm_xmax is None:
            self.mm_size = mm_ymax
            self.mm_to_pixels = self.ymax/self.mm_size
        else:
            logging.warning(
                "Display didn't find millimeter size in argument list or more than one "
                "millimeter size was given. Using default value instead with the height. "
                "Note: Read the doc.")
            self.mm_size = Display.DEFAULT_MM_SIZE
            self.mm_to_pixels = self.ymax/self.mm_size

        self.pixels_to_mm = 1/self.mm_to_pixels

        self.mm_size = mm_size

        self.max = max(self.xmax, self.ymax)
        self.min = min(self.xmax, self.ymax)
        half_min = math.floor(self.min/2)

        self.center = (math.floor(self.xmax/2), math.floor(self.ymax/2))
        self.center_x, self.center_y = self.center

        self.bottom_left = (self.center_x-half_min, self.center_y-half_min)
        self.top_left = (self.center_x-half_min, self.center_y+half_min)
        self.top_right = (self.center_x+half_min, self.center_y+half_min)
        self.bottom_right = (self.center_x+half_min, self.center_y-half_min)

        self.bottom = (math.floor(self.xmax/2), 0)
        self.right = (self.xmax, math.floor(self.ymax/2))
        self.top = (math.floor(self.xmax/2), self.ymax)
        self.left = (0, math.floor(self.ymax/2))

        self.blank_image = np.zeros((self.ymax, self.xmax, 3), np.uint8)

        self.created = False
        self.warning_shown = False
        self.lidar_name = lidar_name

    def createTarget(self):
        cv2.circle(self.blank_image, self.center, radius=3,
                   thickness=3, color=Display.TARGET_COLOR)

        circle_count = math.floor(self.max*self.pixels_to_mm/50)
        radius = math.floor(50 * self.mm_to_pixels)

        for i in range(circle_count):
            cv2.circle(self.blank_image, self.center, radius=radius *
                       i, thickness=1, color=Display.TARGET_COLOR)
            cv2.putText(self.blank_image, f'{50*i}mm', 
                        org=(self.center_x+radius*i, self.center_y+Display.FONT_SIZE*((-1)**i)), 
                        fontFace=Display.FONT,
                        fontScale=Display.FONT_SCALE, color=(255, 255, 255))

        cv2.line(self.blank_image, self.bottom_right,
                 self.top_left, thickness=1, color=Display.TARGET_COLOR)
        cv2.line(self.blank_image, self.bottom_left,
                 self.top_right, thickness=1, color=Display.TARGET_COLOR)
        cv2.line(self.blank_image, self.left, self.right,
                 thickness=1, color=Display.TARGET_COLOR)
        cv2.line(self.blank_image, self.bottom, self.top,
                 thickness=1, color=Display.TARGET_COLOR)

        self.created = True

    def draw(self, measurement: Measurement):
        if not self.created and self.warning_shown is False:
            logging.warning("Display's target is not created")
            self.warning_shown = True
            
        if measurement is None:
            logging.warning("Can't display measurement: it is None")
            return
        
        self.current_image = self.blank_image.copy()
        for i in range(len(measurement.distances)):
            x = math.floor(math.cos(math.radians(
                float(measurement.angles[i])/100)) * measurement.distances[i] * self.mm_to_pixels)
            y = math.floor(math.sin(math.radians(
                float(measurement.angles[i])/100)) * measurement.distances[i] * self.mm_to_pixels)
            # print("angle: {:3f} distance: {:}".format((float(measurement.angles[i])/100)),measurement.distances[i])
            # We use int() to floor the value to the closest integer to zero.
            if (x != 0 and x < int(self.xmax/2) and x > int(-self.xmax/2)) and (y != 0 and y < int(self.ymax/2) and y > int(-self.ymax/2)):
                cv2.circle(self.current_image, (self.center_x+x, self.center_y-y),
                           radius=2, color=(0, 0, 255))

    def showImage(self):
        cv2.imshow(self.lidar_name, self.current_image)
        cv2.waitKey(30)

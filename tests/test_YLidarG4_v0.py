import logging
import time

from tests.Display import Display
from lidars.YLidarG4_v0 import YLidarG4
from lidars.Lidar import Lidar


def start_lidar(port: str) -> Lidar | None:
        
    lidar = YLidarG4(port)
    if not lidar.connect():
        return None
    
    lidar.begin(timeout=3.5)
    time.sleep(2)
    logging.info(f"YLidarG4 infos: {lidar.getInfo()}")
    lidar.setAngularResolution(100)
    lidar.startMeasuring()
    return lidar

def stop_lidar(lidar: Lidar):
    lidar.stopMeasuring()
    lidar.stop()
    lidar.disconnect()
    
import PyLidar3
from lidars.Lidar import Measurement
import datetime
def test_():
    port = "COM8"
    Obj = PyLidar3.YdLidarG4(port)
    if(Obj.Connect()):
        print(Obj.GetDeviceInfo())
        gen = Obj.StartScanning()
        t = time.time() # start time 
        angles = [None]*360
        distances = [None]*360
        display = Display("YLidarG4",(1280,720),(None,1000))
        display.createTarget()
        while (time.time() - t) < 30: #scan for 30 seconds
            data = next(gen)
            for i,(angle, dist) in enumerate(data.items()):
                angles[i] = angle*100
                distances[i] = dist
                
                new_measurement = Measurement(datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S"),
                                            100,
                                            angles,
                                            distances)
            display.draw(new_measurement)
            display.showImage()
        #logging.debug(f'Got {len(new_measurement.angles
        is_plot = False
        Obj.StopScanning()
        Obj.Disconnect()
    else:
        print("Error connecting to device")

def test(port: str):
    display = Display("YLidarG4",(1280,720),(None,1000))
    display.createTarget()

    lidar = start_lidar(port)
    
    if lidar is None:
        return False
    
    for i in range(10000):
        measurement = lidar.getMeasurement()
        
        while measurement == None:
            logging.debug("Waiting for measurement")
            measurement = lidar.getMeasurement()
            time.sleep(0.5)
        
        #logging.debug(measurement)
        display.draw(measurement)
        display.showImage()
        
    return True
            

import logging
import time

from tests.Display import Display
from lidars.LS01B import LS01B
from lidars.Lidar import Lidar

def start_lidar(port) -> Lidar | None:
        
    lidar = LS01B(port)
    if not lidar.connect():
        return None
    lidar.begin()
    time.sleep(2)
    logging.info(f"LS01B infos: {lidar.getInfo()}")
    lidar.setAngularResolution(25)
    lidar.setSpeed(600)
    lidar.startMeasuring()
    return lidar

def stop_lidar(lidar: Lidar):
    lidar.stopMeasuring()
    lidar.stop()
    lidar.disconnect()

def test(port):
    display = Display("LS01B",(1280,720),(None,1000))
    display.createTarget()

    lidar = start_lidar(port)
    
    if lidar is None:
        return False
    
    for i in range(10000):
        measurement = lidar.getMeasurement()
        
        while measurement == None:
            logging.debug("Waiting for measurement")
            measurement = lidar.getMeasurement()
            time.sleep(0.5)
        
        #logging.debug(measurement)
        display.draw(measurement)
        display.showImage()
        
    return True
            

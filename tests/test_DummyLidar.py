import logging
import time

from tests.Display import Display
from lidars.DummyLidar import DummyLidar, DummyMode
from lidars.Lidar import Lidar


def start_lidar(port: str) -> Lidar | None:
        
    lidar = DummyLidar(DummyMode.SINE)
    if not lidar.connect():
        return None
    
    lidar.begin(timeout=3.5)
    time.sleep(2)
    logging.info(f"DummyLidar infos: {lidar.getInfo()}")
    lidar.setAngularResolution(100)
    lidar.startMeasuring()
    return lidar

def stop_lidar(lidar: Lidar):
    lidar.stopMeasuring()
    lidar.stop()
    lidar.disconnect()
    

def test(port: str):
    display = Display("DummyLidar",(1280,720),(None,1000))
    display.createTarget()

    lidar = start_lidar(port)
    
    if lidar is None:
        return False
    
    for i in range(10000):
        measurement = lidar.getMeasurement()
        
        while measurement == None:
            logging.debug("Waiting for measurement")
            measurement = lidar.getMeasurement()
            time.sleep(0.05)
        
        logging.debug(measurement)
        display.draw(measurement)
        display.showImage()
        
    return True
            

# HOW TO USE:
# - Copy this file and rename it to __main__.py
# - DO NOT STAGE A __main__.py FILE
# - Change the line from tests.TEST_FILE import * to the correct test file
# if the logger config file is present, uncomment the corresponding lines


import logging
import os
# import logger # UNCOMMENT IF LOGGER CONFIG FILE IS PRESENT

from tests.TEST_FILE import *


def init():
    """Fist function called"""
    # logger.init_logger() # UNCOMMENT IF LOGGER CONFIG FILE IS PRESENT



def main():

    logging.info("Starting test")
    test()
    logging.info("Test stopped") 
 



def finish():
    """Last function called"""
    # If log file is empty, we delete it
    # if os.stat(logger.LOG_FILE_PATH).st_size == 0:
    #     os.remove(logger.LOG_FILE_PATH)


if __name__ == "__main__":
    init()
    main()
    finish()
